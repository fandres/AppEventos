package com.company.jf.eventosappjf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class EventosHoy extends AppCompatActivity {
    String[] elementos;
    String[] elementosIdEventos;
    String[] elementosNombre;
    String[] elementosDescripcion;
    String[] elementosFecha;
    String[] elementosUniversidad;
    String[] elementosLugar;
    String[] elementosUrlEvento;
    String[] elementosLongitud;
    String[] elementosLatitud;
    Integer success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_hoy);
        //final TextView verTodo = findViewById(R.id.textViewTodo);

        final ListView listaTodo = findViewById(R.id.listaTodo);

        listaTodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(EventosHoy.this, "tesdf"+elementos[position], Toast.LENGTH_LONG).show();
                Intent cambio = new Intent(EventosHoy.this, DetalleActivity.class);
                cambio.putExtra("position",position);
                // se enviatodo el array list
                cambio.putExtra("id_evento",elementosIdEventos[position]);
                cambio.putExtra("nombre",elementosNombre[position]);
                cambio.putExtra("descripcion",elementosDescripcion[position]);
                cambio.putExtra("fecha",elementosFecha[position]);
                cambio.putExtra("universidad",elementosUniversidad[position]);
                cambio.putExtra("lugar",elementosLugar[position]);
                cambio.putExtra("urlEvento",elementosUrlEvento[position]);
                cambio.putExtra("latitud",elementosLatitud[position]);
                cambio.putExtra("longitud",elementosLongitud[position]);

                startActivity(cambio);
            }
        });


                OkHttpClient client = new OkHttpClient();
                Request request = new Request.Builder()
                        .url("http://eventosapp.webcindario.com/getEventos.php")
                        .addHeader("Content-Type", "application/json; charset=utf-8")

                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if(response.isSuccessful()){
                            //tomamos la respuesta del json
                            String rta = response.body().string(); //{"success":1,"message":"Has hecho el reporte existosamente."}


                            Gson gson = new Gson();
                            GetEvento respuestaBackend = new GetEvento();
                            respuestaBackend = gson.fromJson(rta,GetEvento.class);

                            final GetEvento finalRespuestaBackend = respuestaBackend;
                            success = finalRespuestaBackend.getSuccess();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (success == 1)
                                    {
                                    //la listas toca con arreglos
                                    elementos = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosIdEventos = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosNombre = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosDescripcion = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosFecha = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosUniversidad = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosLugar = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosUrlEvento = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosLatitud = new String[finalRespuestaBackend.getEventos().size()];
                                    elementosLongitud = new String[finalRespuestaBackend.getEventos().size()];

                                    String texto = "";
                                    // for para acumular los textos en una sola variable
                                    for (int i =0 ; i<finalRespuestaBackend.getEventos().size(); i++)
                                    {
                                        //texto = texto+"\n"+finalRespuestaBackend.getCaracteristicas().get(i).getCaracteristicas();
                                        elementos[i] = texto+"\n"+finalRespuestaBackend.getEventos().get(i).getNombre()+"\n"+finalRespuestaBackend.getEventos().get(i).getFecha();
                                        elementosIdEventos[i] = texto+finalRespuestaBackend.getEventos().get(i).getIdEvento();
                                        elementosNombre[i] = texto+finalRespuestaBackend.getEventos().get(i).getNombre();
                                        elementosDescripcion[i] = texto+finalRespuestaBackend.getEventos().get(i).getDescripcion();
                                        elementosFecha[i] = texto+"\n"+finalRespuestaBackend.getEventos().get(i).getFecha();
                                        elementosUniversidad[i] = texto+finalRespuestaBackend.getEventos().get(i).getUniversidad();
                                        elementosLugar[i] = texto+finalRespuestaBackend.getEventos().get(i).getLugar();
                                        elementosUrlEvento[i] = texto+finalRespuestaBackend.getEventos().get(i).getUrlEvento();
                                        elementosLatitud[i] = texto+finalRespuestaBackend.getEventos().get(i).getLatitud();
                                        elementosLongitud[i] = texto+finalRespuestaBackend.getEventos().get(i).getLongitud();
                                    }
                                    // secrea elemento adaptar que relaciona los elementos con la lista
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(EventosHoy.this,android.R.layout.simple_list_item_1, elementos);
                                    listaTodo.setAdapter(adapter);

                                    //verTodo.setText(texto);
                                    }
                                    else
                                    {
                                        Toast.makeText(getApplicationContext(),"No hay Eventos para hoy",Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"Error de conexiòn con el servidor :_(",Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}

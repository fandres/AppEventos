package com.company.jf.eventosappjf;

import android.content.Context;

import java.util.ArrayList;

public class AdapterEventos {

    private final Context context;
    private final ArrayList<AdapterEventosList> values;

    public AdapterEventos(Context context, ArrayList<AdapterEventosList> eventos) {
        this.context = context;
        this.values = eventos;
    }
}

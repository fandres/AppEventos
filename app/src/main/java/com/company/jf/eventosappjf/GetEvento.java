package com.company.jf.eventosappjf;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetEvento {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("eventos")
    @Expose
    private List<Evento> eventos = null;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<Evento> getEventos() {
        return eventos;
    }

    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

}
package com.company.jf.eventosappjf;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Usuario {

@SerializedName("success")
@Expose
private Integer success;
@SerializedName("asistencia")
@Expose
private List<Asistencia> asistencia = null;

public Integer getSuccess() {
return success;
}

public void setSuccess(Integer success) {
this.success = success;
}

public List<Asistencia> getAsistencia() {
return asistencia;
}

public void setAsistencia(List<Asistencia> asistencia) {
this.asistencia = asistencia;
}

}
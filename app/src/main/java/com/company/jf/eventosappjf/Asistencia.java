package com.company.jf.eventosappjf;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Asistencia {

    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("fk_id_evento")
    @Expose
    private String fkIdEvento;

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getFkIdEvento() {
        return fkIdEvento;
    }

    public void setFkIdEvento(String fkIdEvento) {
        this.fkIdEvento = fkIdEvento;
    }

}
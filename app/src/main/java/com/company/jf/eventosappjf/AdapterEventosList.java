package com.company.jf.eventosappjf;

import java.io.Serializable;

public class AdapterEventosList implements Serializable {

    String nombre;
    String descripcion;
    String fecha;
    String universidad;
    String latitud;
    String longitud;
    String lugar;
    String urlEvento;

    public AdapterEventosList(String nombre, String descripcion, String fecha, String universidad, String latitud, String longitud, String lugar, String urlEvento) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.universidad = universidad;
        this.latitud = latitud;
        this.longitud = longitud;
        this.lugar = lugar;
        this.urlEvento = urlEvento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getUrlEvento() {
        return urlEvento;
    }

    public void setUrlEvento(String urlEvento) {
        this.urlEvento = urlEvento;
    }
}

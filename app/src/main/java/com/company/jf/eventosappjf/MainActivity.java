package com.company.jf.eventosappjf;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //////
        //shared preferen
        ////

        SharedPreferences sharedPref = getSharedPreferences(
                // aca es el nombre del archivo getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                "misvariables", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        //se puede leer o escribir
      // editor.putInt("entertoprubea",123);

        //obtener la variable. se puede obtener en cualquier activity

        //int numerico = sharedPref.getInt("entertoprubea",34);

        String [] posibilidades= {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"};

        String palabra = sharedPref.getString("stringprines","NoGuarda");
        if (palabra == "NoGuarda")
        {
            String cadena="";
            for(int i=1; i <= 15; i++)
            {
                int numRandon = (int) Math.round(Math.random() * 61 ) ;
                cadena = cadena+posibilidades[numRandon];
            }
            editor.putString("stringprines",cadena);
            editor.commit();
            palabra = sharedPref.getString("stringprines","NoGuarda");
        }

        Button eventHoy = findViewById(R.id.eventHoy);
        Button eventProximo = findViewById(R.id.eventProximo);
        Button eventMios = findViewById(R.id.eventMios);


        eventHoy.setOnClickListener(this);
        eventProximo.setOnClickListener(this);
        eventMios.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.eventHoy:
                Intent intent  = new Intent(MainActivity.this,EventosHoy.class);
                startActivity(intent);
                break;
            case R.id.eventProximo:
                Intent proximo  = new Intent(MainActivity.this,EventosProxActivity.class);
                startActivity(proximo);
                break;
            case R.id.eventMios:
                Intent cambio  = new Intent(MainActivity.this,EventosFavoritosActivity.class);
                startActivity(cambio);
                break;
        }

    }

}

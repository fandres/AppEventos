package com.company.jf.eventosappjf;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DetalleActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    String latitud;
    String longitud;
    String nombre;
    Integer success;
    Button asistir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        TextView nombreText = findViewById(R.id.nombreText);
        TextView descripcionText = findViewById(R.id.descripcionText);
        TextView fechaText = findViewById(R.id.fechaText);
        TextView universidadText = findViewById(R.id.universidadText);
        TextView lugarText = findViewById(R.id.lugarText);


        asistir = findViewById(R.id.butAsistir);
        Button verPagina = findViewById(R.id.butVerpagina);

        ImageView butHome = findViewById(R.id.butHome);
        butHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(DetalleActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

        verPagina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String urlEvento = getIntent().getStringExtra("urlEvento");
                Intent irPagina = new Intent(DetalleActivity.this, PaginaWebEvento.class);
                irPagina.putExtra("url",urlEvento);
                startActivity(irPagina);
            }
        });
        SharedPreferences sharedPref = getSharedPreferences(
                // aca es el nombre del archivo getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                "misvariables", Context.MODE_PRIVATE);
        final String palabra = sharedPref.getString("stringprines","NoGuarda");
        asistir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder = new AlertDialog.Builder(DetalleActivity.this);

                builder.setMessage("Favorito")
                        .setTitle("¿Marcar como Favorito?")
                        .setIcon(R.drawable.favorito)
                        .setCancelable(true)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //////////////
                                //Guardar variables
                                /////////////
                                final String idEvento = getIntent().getStringExtra("id_evento");
                                OkHttpClient client = new OkHttpClient();

                                RequestBody formBody = new FormBody.Builder()
                                        .add("idUser",palabra)
                                        .add("fkIdEvento",idEvento)
                                        .build();

                                Request request = new Request.Builder()
                                        .url("http://eventosapp.webcindario.com/insertAsistencia.php")
                                        .post(formBody)
                                        .build();

                                client.newCall(request).enqueue(new Callback() {
                                    @Override public void onFailure(Call call, IOException e) {
                                        e.printStackTrace();
                                    }

                                    @Override public void onResponse(Call call, Response response) throws IOException {
                                        if(response.isSuccessful()){
                                            String rta = response.body().string(); //{"success":1,"message":"Has hecho el reporte existosamente."}
                                            //////////////////////////////
                                            Gson gson = new Gson();
                                            GetEvento respuestaBackend = new GetEvento();
                                            respuestaBackend = gson.fromJson(rta,GetEvento.class);
                                            final GetEvento finalRespuestaBackend = respuestaBackend;
                                            success = finalRespuestaBackend.getSuccess();
                                            ////////////////////////


                                                    if(rta.contains("existosamente")){

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(getApplicationContext(),"Registro exitoso :)",Toast.LENGTH_LONG).show();

                                                            }
                                                        });

                                                    }
                                                    else if(success == 2)
                                                    {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                Toast.makeText(getApplicationContext(),"Ya esta en favoritos :@",Toast.LENGTH_LONG).show();

                                                            }
                                                        });
                                                    }


                                        }
                                    }
                                });
                            }
                        })
                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        final int position = getIntent().getIntExtra("position",0);
        final String descripcion = getIntent().getStringExtra("descripcion");
        nombre = getIntent().getStringExtra("nombre");
        final String fecha = getIntent().getStringExtra("fecha");
        final String universidad = getIntent().getStringExtra("universidad");
        final String lugar = getIntent().getStringExtra("lugar");



        //recoge position
        nombreText.setText(nombre);
        descripcionText.setText(descripcion);
        fechaText.setText(fecha);
        universidadText.setText(universidad);
        lugarText.setText(lugar);
        // final String idEvento = getIntent().getStringExtra("id_evento");
        //id.setText(""+idEvento);

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        latitud = getIntent().getStringExtra("latitud");
        longitud = getIntent().getStringExtra("longitud");
        nombre = getIntent().getStringExtra("nombre");
        Double latitudInt = Double.parseDouble(latitud);
        Double longitudInt = Double.parseDouble(longitud);
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(latitudInt, longitudInt);
        mMap.addMarker(new MarkerOptions().position(sydney).title(""+nombre));
        float zoomLevel = 16.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, zoomLevel));
    }
}

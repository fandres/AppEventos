package com.company.jf.eventosappjf;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class EventosProxActivity extends AppCompatActivity {
    String[] elementos2;
    String[] elementosIdEvento2;
    String[] elementosNombre2;
    String[] elementosDescripcion2;
    String[] elementosFecha2;
    String[] elementosUniversidad2;
    String[] elementosLugar2;
    String[] elementosUrlEvento2;
    String[] elementosLongitud2;
    String[] elementosLatitud2;
    Integer success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_hoy);
        //final TextView verTodo = findViewById(R.id.textViewTodo);

        final ListView listaTodo = findViewById(R.id.listaTodo);

        listaTodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(EventosHoy.this, "tesdf"+elementos[position], Toast.LENGTH_LONG).show();
                Intent cambio = new Intent(EventosProxActivity.this, DetalleActivity.class);
                cambio.putExtra("position",position);
                // se enviatodo el array list
                cambio.putExtra("id_evento",elementosIdEvento2[position]);
                cambio.putExtra("nombre",elementosNombre2[position]);
                cambio.putExtra("descripcion",elementosDescripcion2[position]);
                cambio.putExtra("fecha",elementosFecha2[position]);
                cambio.putExtra("universidad",elementosUniversidad2[position]);
                cambio.putExtra("lugar",elementosLugar2[position]);
                cambio.putExtra("urlEvento",elementosUrlEvento2[position]);
                cambio.putExtra("latitud",elementosLatitud2[position]);
                cambio.putExtra("longitud",elementosLongitud2[position]);

                startActivity(cambio);
            }
        });


        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://eventosapp.webcindario.com/getEventosProx.php")
                .addHeader("Content-Type", "application/json; charset=utf-8")

                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    //tomamos la respuesta del json
                    String rta = response.body().string(); //{"success":1,"message":"Has hecho el reporte existosamente."}

                    // tener en cuenta que la clase rta se llama igual que el string :_(
                    Gson gson = new Gson();
                    GetEvento respuestaBackend = new GetEvento();
                    respuestaBackend = gson.fromJson(rta,GetEvento.class);

                    final GetEvento finalRespuestaBackend = respuestaBackend;
                    success = finalRespuestaBackend.getSuccess();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (success == 1)
                            {
                            //la listas toca con arreglos
                            elementos2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosIdEvento2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosNombre2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosDescripcion2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosFecha2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosUniversidad2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosLugar2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosUrlEvento2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosLatitud2 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosLongitud2 = new String[finalRespuestaBackend.getEventos().size()];

                            String texto = "";
                            // for para acumular los textos en una sola variable
                            for (int i =0 ; i<finalRespuestaBackend.getEventos().size(); i++)
                            {
                                //texto = texto+"\n"+finalRespuestaBackend.getCaracteristicas().get(i).getCaracteristicas();
                                elementos2[i] = texto+"\n"+finalRespuestaBackend.getEventos().get(i).getNombre()+"\n"+finalRespuestaBackend.getEventos().get(i).getFecha();
                                elementosIdEvento2[i] = texto+finalRespuestaBackend.getEventos().get(i).getIdEvento();
                                elementosNombre2[i] = texto+finalRespuestaBackend.getEventos().get(i).getNombre();
                                elementosDescripcion2[i] = texto+finalRespuestaBackend.getEventos().get(i).getDescripcion();
                                elementosFecha2[i] = texto+finalRespuestaBackend.getEventos().get(i).getFecha();
                                elementosUniversidad2[i] = texto+finalRespuestaBackend.getEventos().get(i).getUniversidad();
                                elementosLugar2[i] = texto+finalRespuestaBackend.getEventos().get(i).getLugar();
                                elementosUrlEvento2[i] = texto+finalRespuestaBackend.getEventos().get(i).getUrlEvento();
                                elementosLatitud2[i] = texto+finalRespuestaBackend.getEventos().get(i).getLatitud();
                                elementosLongitud2[i] = texto+finalRespuestaBackend.getEventos().get(i).getLongitud();
                            }
                            // secrea elemento adaptar que relaciona los elementos con la lista
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(EventosProxActivity.this,android.R.layout.simple_list_item_1, elementos2);
                            listaTodo.setAdapter(adapter);

                            //verTodo.setText(texto);
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),"No hay eventos registrados",Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Error de conexiòn con el servidor :_(",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

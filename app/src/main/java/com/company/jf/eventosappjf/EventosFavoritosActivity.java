package com.company.jf.eventosappjf;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class EventosFavoritosActivity extends AppCompatActivity {
    String[] elementos3;
    String[] elementosIdEventos3;
    String[] elementosNombre3;
    String[] elementosDescripcion3;
    String[] elementosFecha3;
    String[] elementosUniversidad3;
    String[] elementosLugar3;
    String[] elementosUrlEvento3;
    String[] elementosLongitud3;
    String[] elementosLatitud3;
    String rta;
    Integer success;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_favoritos);

        final ListView listaTodo = findViewById(R.id.listaTodo);

        listaTodo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Toast.makeText(EventosHoy.this, "tesdf"+elementos[position], Toast.LENGTH_LONG).show();
                Intent cambio = new Intent(EventosFavoritosActivity.this, DetalleActivity.class);
                cambio.putExtra("position",position);
                // se enviatodo el array list
                cambio.putExtra("id_evento",elementosIdEventos3[position]);
                cambio.putExtra("nombre",elementosNombre3[position]);
                cambio.putExtra("descripcion",elementosDescripcion3[position]);
                cambio.putExtra("fecha",elementosFecha3[position]);
                cambio.putExtra("universidad",elementosUniversidad3[position]);
                cambio.putExtra("lugar",elementosLugar3[position]);
                cambio.putExtra("urlEvento",elementosUrlEvento3[position]);
                cambio.putExtra("latitud",elementosLatitud3[position]);
                cambio.putExtra("longitud",elementosLongitud3[position]);

                startActivity(cambio);
            }
        });
        SharedPreferences sharedPref = getSharedPreferences(
                // aca es el nombre del archivo getString(R.string.preference_file_key), Context.MODE_PRIVATE);
                "misvariables", Context.MODE_PRIVATE);
        final String palabra = sharedPref.getString("stringprines","NoGuarda");

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("http://eventosapp.webcindario.com/getAsistencia.php?idUser="+palabra)
                .addHeader("Content-Type", "application/json; charset=utf-8")

                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    //tomamos la respuesta del json
                     rta = response.body().string(); //{"success":1,"message":"Has hecho el reporte existosamente."}

                    // tener en cuenta que la clase rta se llama igual que el string :_(
                    Gson gson = new Gson();
                    GetEvento respuestaBackend = new GetEvento();
                    respuestaBackend = gson.fromJson(rta,GetEvento.class);

                    final GetEvento finalRespuestaBackend = respuestaBackend;
                    success = finalRespuestaBackend.getSuccess();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (success == 1)
                            {

                            //la listas toca con arreglos
                            elementos3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosIdEventos3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosNombre3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosDescripcion3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosFecha3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosUniversidad3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosLugar3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosUrlEvento3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosLatitud3 = new String[finalRespuestaBackend.getEventos().size()];
                            elementosLongitud3 = new String[finalRespuestaBackend.getEventos().size()];

                            String texto = "";
                            // for para acumular los textos en una sola variable
                            for (int i =0 ; i<finalRespuestaBackend.getEventos().size(); i++)
                            {
                                //texto = texto+"\n"+finalRespuestaBackend.getCaracteristicas().get(i).getCaracteristicas();
                                elementos3[i] = texto+"\n"+finalRespuestaBackend.getEventos().get(i).getNombre()+"\n"+finalRespuestaBackend.getEventos().get(i).getFecha();
                                elementosIdEventos3[i] = texto+finalRespuestaBackend.getEventos().get(i).getIdEvento();
                                elementosNombre3[i] = texto+finalRespuestaBackend.getEventos().get(i).getNombre();
                                elementosDescripcion3[i] = texto+finalRespuestaBackend.getEventos().get(i).getDescripcion();
                                elementosFecha3[i] = texto+finalRespuestaBackend.getEventos().get(i).getFecha();
                                elementosUniversidad3[i] = texto+finalRespuestaBackend.getEventos().get(i).getUniversidad();
                                elementosLugar3[i] = texto+finalRespuestaBackend.getEventos().get(i).getLugar();
                                elementosUrlEvento3[i] = texto+finalRespuestaBackend.getEventos().get(i).getUrlEvento();
                                elementosLatitud3[i] = texto+finalRespuestaBackend.getEventos().get(i).getLatitud();
                                elementosLongitud3[i] = texto+finalRespuestaBackend.getEventos().get(i).getLongitud();
                            }
                            // secrea elemento adaptar que relaciona los elementos con la lista
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(EventosFavoritosActivity.this,android.R.layout.simple_list_item_1, elementos3);
                            listaTodo.setAdapter(adapter);


                            //verTodo.setText(texto);
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(),"No has asignado eventos favoritos :/",Toast.LENGTH_LONG).show();
                            }
                        }

                    });

                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Error de conexiòn con el servidor :_(",Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

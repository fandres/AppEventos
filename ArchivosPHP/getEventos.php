<?php

/*
 *  El siguiente codigo sirve para que de una categoria N que se seleccione
* me de todas las caracterisiticas que hay de esa categoria seleccionada
 */

// array for JSON response
$response = array();

// include db connect class
require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/db_config.php';

// connecting to db
    //$db = new DB_CONNECT();
	 $mysqli = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_DATABASE);

// connecting to db
$db = new DB_CONNECT();

// check for post data(revisar si hay datos posteriores)

  date_default_timezone_set('America/Bogota');
    $hoy = date("Y-m-d");

    

    // obtener de categorias (N =pid) de esa categoria N traigame las caracteristicas e imagenes que tiene
    $result = mysqli_query($mysqli,"SELECT id_evento,nombre,descripcion,fecha,universidad,latitud,longitud,lugar,urlEvento FROM eventos WHERE fecha = '$hoy'");
	//print_r($result);
	//echo "SELECT id_evento,nombre,descripcion,fecha,universidad,latitud,longitud,lugar,urlEvento FROM eventos WHERE fecha = '$hoy'";
		
    if (!empty($result)) {
        // check for empty result
        if (mysqli_num_rows($result) > 0) {


			$return_arr = array();

			  // success
            $response["success"] = 1;

            // user node
            $response["eventos"] = array();


           while ($fila = mysqli_fetch_array($result)){
		$fila_array['id_evento']= utf8_encode($fila['id_evento']);
               $fila_array['nombre']= utf8_encode($fila['nombre']);
               $fila_array['descripcion'] = utf8_encode($fila['descripcion']);
               $fila_array['fecha'] = utf8_encode($fila['fecha']);
               $fila_array['universidad'] = utf8_encode($fila['universidad']);
               $fila_array['latitud'] = utf8_encode($fila['latitud']);
               $fila_array['longitud'] = utf8_encode($fila['longitud']);
               $fila_array['lugar'] = utf8_encode($fila['lugar']);
               $fila_array['urlEvento'] = utf8_encode($fila['urlEvento']);

			    array_push($response["eventos"] ,$fila_array);
			    }




            // echoing JSON response
            echo json_encode($response);
        } else {
            // Evento no encontrado
            $response["success"] = 0;
            $response["message"] = "Eventos no encontrados";

            // echo no users JSON
            echo json_encode($response);
        }
    } else {
        // Evento no encontrado
        $response["success"] = 0;
        $response["message"] = "Eventos no encontrados";

        // echo no users JSON
        echo json_encode($response);
    }
 
?>
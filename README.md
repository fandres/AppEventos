Desarrollado en: Android Studio, PHP, JSON, FireBase 


Aplicación desarrollada para mostrar los diferentes eventos academicos realizados por las Universidades de Santander. 

La app consume servicios de un servidor el cual devuelve en formato JSON las diferentes consultas programadas en PHP. 
Esta aplicación también hace uso de push notification con onesignal para notificar a los usuario sobre eventos destacados.